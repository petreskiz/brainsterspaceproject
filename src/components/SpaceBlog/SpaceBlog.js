import React, { useState, useEffect } from "react";
import "./SpaceBlog.css";
import {
  Navbar,
  Nav,
  Carousel,
  Row,
  Col,
  Badge,
  Button
} from "react-bootstrap";
import Picture1 from "../../Sliki/Space_Blog_Kartici/1_xX6Cpskem0SbqQuPZ8R-4g.jpeg";
import Picture2 from "../../Sliki/Space_Blog_Kartici/20191128_141433810_iOS-1200x600.jpg";
import Picture3 from "../../Sliki/Space_Blog_Kartici/instruktori.jpg";
import Picture4 from "../../Sliki/Space_Blog_Kartici/OFIS LEASURE_01_1.jpg";
import Picture5 from "../../Sliki/Space_Blog_Kartici/Team-Picture-1200x600.jpg";
import Picture6 from "../../Sliki/Space_Blog_Kartici/Untitled-design-5-1200x600.jpg";
import Card from "../HomePage/Card/Card";
import icon1 from "./arrow-right-circle.svg";
import icon2 from "./arrow-left-circle.svg";

const SpaceBlog = props => {
  const [data] = useState([
    {
      id: 1,
      picture: Picture1,
      title: "Deep Dive семинари кои ќе овозможуваат брз кариерен напредок",
      text:
        "Нов продукт од Braister во Braister Space. Интензивни дводневни семинари кои ќе ти помогнат од Mid level брзо да го постигнеш твојот Senior напредок.",
      category: "Кариера"
    },
    {
      id: 2,
      picture: Picture2,
      title:
        "Ако не те бива за програмирање дали можеш да имаш кариера во Tech?",
      text:
        "Нов продукт од Braister во Braister Space. Интензивни дводневни семинари кои ќе ти помогнат од Mid level брзо да го постигнеш твојот Senior напредок.",
      category: "Кариера"
    },
    {
      id: 3,
      picture: Picture4,
      title: "Што значи денес, твојата канцеларија да е smart?",
      text:
        "Нов продукт од Braister во Braister Space. Интензивни дводневни семинари кои ќе ти помогнат од Mid level брзо да го постигнеш твојот Senior напредок.",
      category: "Технологија"
    },
    {
      id: 4,
      picture: Picture1,
      title: "Како до кариера во Data Science?",
      text:
        "Нов продукт од Braister во Braister Space. Интензивни дводневни семинари кои ќе ти помогнат од Mid level брзо да го постигнеш твојот Senior напредок.",
      category: "Data Science"
    },
    {
      id: 5,
      picture: Picture3,
      title:
        "Интервју со Никола Ванчев, арт директор во Tank, Estonia. #SpaceFamily",
      text:
        "Нов продукт од Braister во Braister Space. Интензивни дводневни семинари кои ќе ти помогнат од Mid level брзо да го постигнеш твојот Senior напредок.",
      category: "Маркетинг"
    },
    {
      id: 6,
      picture: Picture6,
      title: "Зошто дизајн, накратко со Дијана Димитриевска #SpaceFamily",
      text:
        "Нов продукт од Braister во Braister Space. Интензивни дводневни семинари кои ќе ти помогнат од Mid level брзо да го постигнеш твојот Senior напредок.",
      category: "Дизајн"
    },
    {
      id: 7,
      picture: Picture6,
      title: "Што е потребно за добар мобилен дизајн",
      text:
        "Нов продукт од Braister во Braister Space. Интензивни дводневни семинари кои ќе ти помогнат од Mid level брзо да го постигнеш твојот Senior напредок.",
      category: "Дизајн"
    },
    {
      id: 8,
      picture: Picture5,
      title:
        "Македонско-Американската компанија Upshift доби инвестиција од 3.7 милиони...",
      text:
        "Нов продукт од Braister во Braister Space. Интензивни дводневни семинари кои ќе ти помогнат од Mid level брзо да го постигнеш твојот Senior напредок.",
      category: "Impact"
    },
    {
      id: 9,
      picture: Picture2,
      title: "Локалната апликација Challenger и нејзиниот импакт. #SpaceFamily",
      text:
        "Нов продукт од Braister во Braister Space. Интензивни дводневни семинари кои ќе ти помогнат од Mid level брзо да го постигнеш твојот Senior напредок.",
      category: "Impact"
    }
  ]);
  const [filterdData, setfilterData] = useState(data);
  const [active, setActive] = useState([]);

  useEffect(() => {}, [filterdData]);
  useEffect(() => {}, [active]);

  const FilterFunction = e => {
    const cat = e.target.text;

    const activeArray =
      cat === "Сите"
        ? []
        : active.includes(cat)
        ? active.filter(el => el !== cat)
        : active.concat(cat);

    const filterItems =
      activeArray.length === 0
        ? data
        : data.filter(item => activeArray.includes(item.category));

    setfilterData(filterItems);

    setActive(activeArray);
  };

  const [PrevNext] = useState({
    nextIcon: <img src={icon1} alt="" />,
    prevIcon: <img src={icon2} alt="" />
  });

  return (
    <div>
      <Navbar bg="light" expand="sm" className="SpaceBlogNavbar">
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto CustomNAv">
            <p className="m-auto">Филтрирај</p>
            <Nav.Link
              href="#home"
              className={active.length === 0 ? "ac" : ""}
              onClick={FilterFunction}
            >
              Сите
            </Nav.Link>
            <Nav.Link
              href="#link"
              className={active.includes("Кариера") ? "ac" : ""}
              onClick={FilterFunction}
            >
              Кариера
            </Nav.Link>
            <Nav.Link
              href="#link"
              className={active.includes("Технологија") ? "ac" : ""}
              onClick={FilterFunction}
            >
              Технологија
            </Nav.Link>
            <Nav.Link
              href="#link"
              className={active.includes("Data Science") ? "ac" : ""}
              onClick={FilterFunction}
            >
              Data Science
            </Nav.Link>
            <Nav.Link
              href="#link"
              className={active.includes("Маркетинг") ? "ac" : ""}
              onClick={FilterFunction}
            >
              Маркетинг
            </Nav.Link>
            <Nav.Link
              href="#link"
              className={active.includes("Дизајн") ? "ac" : ""}
              onClick={FilterFunction}
            >
              Дизајн
            </Nav.Link>
            <Nav.Link
              href="#link"
              className={active.includes("Impact") ? "ac" : ""}
              onClick={FilterFunction}
            >
              Impact
            </Nav.Link>
            <Nav.Link
              href="https://medium.com/wearelaika/brainster-space-the-
              New-home-of-the-local-tech-community-
              In-skopje-ffe97b564152"
              target="_Blank"
            >
              #SpaceFamily
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Carousel nextIcon={PrevNext.nextIcon} prevIcon={PrevNext.prevIcon}>
        {data.map((el, i) => (
          <Carousel.Item key={i}>
            <div className="bgBlack">
              <img className=" w-100" src={el.picture} alt="First slide" />
            </div>
            <Carousel.Caption>
              <Badge variant="warning">{el.category}</Badge>
              <h1 className="SpaceBlogCarouselTitle">{el.title}</h1>
              <p className="SpaceBlogCarouselText">{el.text}</p>
              <div className="CustomControlIcons">
                <Button variant="light">Дознај повеќе</Button>
                
              </div>
            </Carousel.Caption>
          </Carousel.Item>
        ))}
      </Carousel>
      <Row className="SpaceBlogCardsRow">
        {filterdData.map((el, i) => (
          <Col md={4} sm={6} xs={12} key={i}>
            <Card title={el.title} picture={el.picture} text={el.text}>
              <Badge variant="warning">{el.category}</Badge>
            </Card>
          </Col>
        ))}
      </Row>
    </div>
  );
};

export default SpaceBlog;
