import React from "react";
import "./Academies.css";
import { Row, Col } from "react-bootstrap";
import Picture0 from "../../Sliki/prostorzanastani.jpg";
import Picture1 from "../../Sliki/Renderi/2.jpg";
import Picture2 from "../../Sliki/partnerstvasotechkomp.jpg";
import Picture3 from "../../Sliki/KITCHEN_03.jpg";
import Picture4 from "../../Sliki/prostorzanastani.jpg";
import Picture5 from "../../Sliki/IMG_7707.jpg";
import AcademyCard from "./AcademyCard";

const Academies = props => {
  return (
    <div>
      <Row className="EventSpaceRow">
        <Col md={4} sm={12} xs={12}>
          <h1 className="EventSpaceTitle">Академии</h1>
          <p className="EventSpaceText">
            Нашите академии ќе ти помогнат да ги совладаш најбараните вештини на
            денешницата преку интерактивна настава и практична настава на реални
            проекти.
          </p>
          <p className="EventSpaceText">
            Инструктори кои се докажани експерти во својата област. Преку нив
            имаш можност да ги научиш и предвидиш предизвиците на иднината на
            работење и да се поврзеш со сегашните и идни врвни професионалци и
            компании.
          </p>
          <p className="EventSpaceText">
            Академиите ќе ти го отворат патот кон кариера каква што посакуваш.
          </p>
        </Col>
        <Col md={8} sm={12} xs={12}>
          <img src={Picture0} alt="" width="100%" />
        </Col>
      </Row>
      <Row className="EventSpaceRow">
        <Col md={4} sm={6} xs={12}>
          <AcademyCard
            picture={Picture3}
            title="Академија за графички дизајн"
            bgColor="rgba(128, 0, 128, 0.493)"
            link=""
          />
        </Col>
        <Col md={4} sm={6} xs={12}>
          <AcademyCard
            picture={Picture1}
            title="Академија за дигитален маркетинг"
            bgColor="rgba(182, 4, 4, 0.541)"link=""

          />
        </Col>
        <Col md={4} sm={6} xs={12}>
          <AcademyCard
            picture={Picture2}
            title="Академија за Front-end програмирање"
            bgColor="rgba(0, 128, 0, 0.5)"link=""
          />
        </Col>

        <Col md={4} sm={6} xs={12}>
          <AcademyCard
            picture={Picture0}
            title="Академија за Full-stack програмирање"
            bgColor="rgba(0, 128, 0, 0.479)"link=""
          />
        </Col>
        <Col md={4} sm={6} xs={12}>
          <AcademyCard
            picture={Picture4}
            title="Академија за Data science"
            bgColor="rgba(4, 4, 143, 0.541)"link=""
          />
        </Col>
        <Col md={4} sm={6} xs={12}>
          <AcademyCard
            picture={Picture5}
            title="Академија за software testing"
            bgColor="rgba(21, 83, 21, 0.479)"link=""
          />{" "}
        </Col>
      </Row>
    </div>
  );
};

export default Academies;
