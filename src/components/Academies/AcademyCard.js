import React from "react";
import "./AcademyCard.css";
import { Card } from "react-bootstrap";
import Icon from "../../Sliki/users.svg";
import Icon1 from "../../Sliki/clock.svg";
import Icon2 from "../../Sliki/framer.svg";
import Icon3 from "../../Sliki/calendar.svg";

const AcademyCard = props => {
  return (
    <Card
      className="CardAcademy"
      style={{ backgroundImage: `url(${props.picture})` }}
    >
      <div
        className="OvarlayDiv"
        style={{ backgroundColor: `${props.bgColor}` }}
      >
        <a href={props.link} className="AcademyLink">
          <Card.Title className="AcademyCardTitle">{props.title}</Card.Title>
          <Card.Text className="AcademyCardText">
            <span>
              {" "}
              <img src={Icon} alt="" /> Слободни места: 4
            </span>
            <span>
              {" "}
              <img src={Icon1} alt="" /> Уписи до: 26.08.2019
            </span>
            <span>
              <img src={Icon2} alt="" /> Стани дизајнер за 7 месеци
            </span>
            <span>
              {" "}
              <img src={Icon3} alt="" /> Партнери за вработување: 5{" "}
            </span>
          </Card.Text>
        </a>
      </div>
    </Card>
  );
};

export default AcademyCard;
