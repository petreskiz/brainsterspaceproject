import React from "react";
import "./EventSpace.css";
import { Row, Col, Button, Badge } from "react-bootstrap";
import Picture0 from "../../Sliki/prostorzanastani.jpg";
import Picture1 from "../../Sliki/Renderi/2.jpg";
import Picture2 from "../../Sliki/Renderi/C1 1.jpg";
import Picture3 from "../../Sliki/Renderi/C1 4.jpg";
import Picture4 from "../../Sliki/Renderi/C2 1.jpg";
import Picture5 from "../../Sliki/Renderi/C3 2.jpg";
import Picture6 from "../../Sliki/Renderi/C3 EXIBITION_1.jpg";
import Picture7 from "../../Sliki/Renderi/HOL KON SEDENJE.jpg";
import Picture8 from "../../Sliki/KITCHEN_03.jpg";
import Picture10 from "../../Sliki/Space_Kitchen_Galerija/IMG_7777.jpg";
import Picture11 from "../../Sliki/Space_Kitchen_Galerija/IMG_7635.jpg";
import Picture12 from "../../Sliki/Space_Kitchen_Galerija/IMG_7385.jpg";
import Picture13 from "../../Sliki/Space_Kitchen_Galerija/IMG_7362.jpg";

const EventSpace = () => {
  return (
    <div>
      <Row className="EventSpaceRow">
        <Col md={4} sm={12} xs={12}>
          <h1 className="EventSpaceTitle">Простор за настани</h1>
          <p className="EventSpaceText">
            Нашиот простор се прилагодува според потребите на вашиот настан.
            Седум различни простории и Space Kithen.
          </p>
          <p className="EventSpaceText">
            Наменски создадени да се прилагодуваат и менуваат во согласност со
            типот на настан кој го организирате.
          </p>
          <p className="EventSpaceText">
            Организираме конференции до 150 учесници и обуки и предавања за
            групи до 20 учесници. Контактирај те не за да ви хостираме одличен
            настан.
          </p>
          <Button className="JoinUsButton">
            <i className="fas fa-plus JoinUsButtonPlus"></i>Букирај не
          </Button>
        </Col>
        <Col md={8} sm={12} xs={12}>
          <img src={Picture0} alt="" width="100%" />
        </Col>
      </Row>
      <Row className="EventSpaceRow">
        <Col md={3} sm={12} xs={12}>
          <h1 className="EventSpaceTitle">Нашите простории</h1>
          <p className="EventSpaceText">
            Комплетно адаптибилни. Една сала за 150 учесници или три помали сали
            за групи од по 50 учесника. Училници за од 25 до 40 учесника. Избор
            од две локации.
          </p>
          <p className="EventSpaceText">
            Пулт за прием. И најважното место за networking-Brainster Kitchen.
          </p>
          <p className="EventSpaceText">
            Како го замислувате вашиот следен настан?
          </p>
        </Col>
        <Col md={9} sm={12} xs={12} className="KitchenCol">
          <Col md={4} sm={6} xs={12}>
            <img src={Picture1} alt="" width="100%" height="150px" />
            <p>Brainster</p>
          </Col>
          <Col md={4} sm={6} xs={12}>
            <img src={Picture2} alt="" width="100%" height="150px" />
            <p>Конференциска сала</p>
          </Col>
          <Col md={4} sm={6} xs={12}>
            <img src={Picture3} alt="" width="100%" height="150px" />
            <p>Сала со бина</p>
          </Col>
          <Col md={4} sm={6} xs={12}>
            <img src={Picture4} alt="" width="100%" height="150px" />
            <p>Адаптирана училница</p>
          </Col>
          <Col md={4} sm={6} xs={12}>
            <img src={Picture5} alt="" width="100%" height="150px" />
            <p>Училница</p>
          </Col>
          <Col md={4} sm={6} xs={12}>
            <img src={Picture6} alt="" width="100%" height="150px" />
            <p>Училница</p>
          </Col>
          <Col md={4} sm={6} xs={12}>
            <img src={Picture7} alt="" width="100%" height="150px" />
            <p>Хол</p>
          </Col>
          <Col md={4} sm={6} xs={12}>
            <img src={Picture3} alt="" width="100%" height="150px" />
            <p>Канцелариски простор</p>
          </Col>
          <Col md={4} sm={6} xs={12}>
            <img src={Picture8} alt="" width="100%" height="150px" />
            <p>Space Kitchen</p>
          </Col>
        </Col>
      </Row>
      <Row className="EventSpaceRow">
        <Col md={8} sm={6} xs={12}>
          <Row className="">
            <Col md={6} sm={6} xs={12} className="mb20">
              <img
                className="img-fluid m-x-auto d-block img-responsive"
                src={Picture10}
                alt=""
                width="100%"
              />
              <br />
              <div className="row">
                <div className="col-sm-6 mb20">
                  <img
                    className=" d-block img-responsive"
                    src={Picture11}
                    alt=""
                    width="100%"
                  />
                </div>
                <div className="col-sm-6">
                  <img
                    alt=""
                    className="img-fluid m-x-auto d-block img-responsive"
                    src={Picture12}
                  />
                </div>
              </div>
            </Col>
            <div className="col-sm-6">
              <img
                className="img-fluid m-x-auto d-block img-responsive"
                src={Picture13}
                alt=""
              />
            </div>
          </Row>
        </Col>
        <Col md={4} sm={6} xs={12}>
          <h1>Space Kitchen</h1>
          <p>
            Место каде сите настани започнуваат и завршуваат. Место каде нашиот
            тим готви и експрериментира. Нашата кујна има само едно правило.
          </p>
          <p>
            Храната мора да биде вегетаријанска. Пијалок, кафе или мезе. Вие
            одберете вид на кетеринг ние ќе го обезбедиме.
          </p>
        </Col>
      </Row>
      <Row className="EventSpaceRow RowBadge ">
        <h1>Нудиме</h1>
        <div className="BadgeDiv">
          <Badge className="BadgeEventSpace">
            <i className="fab fa-windows BadgeIcon"></i>Простор
          </Badge>
          <Badge className="BadgeEventSpace">
            <i className="fab fa-sketch BadgeIcon"></i>Space Kitchen
          </Badge>
          <Badge className="BadgeEventSpace">
            <i className="fas fa-clipboard-list BadgeIcon"></i>Логистика
          </Badge>
          <Badge className="BadgeEventSpace">
            <i className="fab fa-servicestack BadgeIcon"></i>Техничка поддршка
          </Badge>
          <Badge className="BadgeEventSpace">
            <i className="fas fa-volume-up BadgeIcon"></i>Звук
          </Badge>
          <Badge className="BadgeEventSpace">
            <i className="far fa-lightbulb BadgeIcon"></i>Светло
          </Badge>
          <Badge className="BadgeEventSpace">
            <i className="fas fa-sitemap BadgeIcon"></i>Помош при Организација
          </Badge>
          <Badge className="BadgeEventSpace">
            <i className="fas fa-video BadgeIcon"></i>Видео и Фотографија
          </Badge>
          <Badge className="BadgeEventSpace">
            <i className="fab fa-facebook BadgeIcon"></i>Промоција на Социјални
            Мрежи
          </Badge>
        </div>
      </Row>
      <Row className="EventSpaceRow">
        <Col md={3} sm={6} xs={12}>
          <h1>Event Host</h1>
          <p>Ања Макеска</p>
          <p>anja@brainster.co</p>
          <p>+389 (0)70 233 414</p>
        </Col>
        <Col md={{ span: 4, offset: 5 }} sm={6} xs={12}>
          <img
            className="img-fluid m-x-auto d-block img-responsive"
            src={Picture13}
            alt=""
          />
        </Col>
      </Row>
    </div>
  );
};

export default EventSpace;
