import React from "react";
import "./Footer.css";
import { Row, Col, Popover, OverlayTrigger } from "react-bootstrap";
import { Link } from "react-router-dom";
import Logo from "../../Sliki/brainster_space_logo.svg";

const popover = (
  <Popover id="popover-basic">
    <Popover.Title as="h3">Контакт</Popover.Title>
    <Popover.Content>
      Емаил: anja@brainster.co <br></br> Телефон: 070 233 414
    </Popover.Content>
  </Popover>
);

const Footer = () => {
  return (
    <div className="FooterDiv  Margin-Top">
      <Row className="FooterRow">
        <Col md={2} sm={4} className="mrg-footer order2">
          <h3 className="FooterTitles">Корисни Линкови</h3>
          <ul className="FooterUl">
            <OverlayTrigger trigger="click" placement="right" overlay={popover}>
              <span className="FooterLink">
                <li className="FooterLi">Контакт</li>
              </span>
            </OverlayTrigger>
            <a
              href="https://www.wearelaika.com/"
              className="FooterLink"
              target="_Blank"
              rel="noopener noreferrer"
            >
              <li className="FooterLi">Отворени позиции</li>
            </a>
            <a
              href="https://medium.com/wearelaika/brainster-space-the-"
              target="_Blank"
              className="FooterLink"
              rel="noopener noreferrer"
            >
              <li className="FooterLi">Галерија</li>
            </a>
            <Link to="/events" className="FooterLink">
              <li className="FooterLi">Календар</li>
            </Link>
          </ul>
        </Col>
        <Col md={2} sm={4} className="mrg-footer order1">
          <h3 className="FooterTitles">Социјални Мрежи</h3>
          <Row>
            <Col md={4} sm={4} xs={4}>
              <a
                href="https://www.facebook.com/brainster.co"
                rel="noopener noreferrer"
              >
                <i className="fab fa-facebook FooterSocialMedia"></i>
              </a>
            </Col>
            <Col md={4} sm={4} xs={4}>
              <a
                href="https://www.linkedin.com/school/brainster-co/"
                rel="noopener noreferrer"
              >
                <i className="fab fa-linkedin FooterSocialMedia"></i>
              </a>
            </Col>
            <Col md={4} sm={4} xs={4}>
              <a
                href="https://www.instagram.com/brainsterco/"
                rel="noopener noreferrer"
              >
                <i className="fab fa-instagram FooterSocialMedia"></i>
              </a>
            </Col>
          </Row>
        </Col>
        <Col
          md={{ span: 2, offset: 6 }}
          sm={4}
          xs={5}
          className="LogoFooter mrg-footer order3"
        >
          <Link to="/">
            <img src={Logo} alt="" width="50%" />
          </Link>
        </Col>
      </Row>
      <Col className="FooterCopyRight">
        <h6>COPYRIGHT@BrainsterSpace. All Rights Reserved</h6>
      </Col>
    </div>
  );
};

export default Footer;
