import React from "react";
import logo from "./brainster_space_logo.svg";
import "./Header.css";
import { Navbar, Nav, Button, Container, Modal, Form } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { useState } from "react";

const Header = props => {
  const [JoinUs, setJoinUs] = useState(false);

  const CloseJoinUs = () => setJoinUs(false);
  const ShowJoinUs = () => setJoinUs(true);

  const [validated, setValidated] = useState(false);
  const handleSubmit = event => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    setValidated(true);
  };

 

  return (
    <Container fluid>
      <Modal show={JoinUs} onHide={CloseJoinUs} className="PartnerModal">
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
          <Modal.Header closeButton>
            <Modal.Title>Приклучи се</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group
              controlId="validationCustomUsername"
              className="FormGroupCustom"
            >
              <Form.Label>Име и Презиме (задолжително)</Form.Label>
              <Form.Control
                type="text"
                aria-describedby="inputGroupPrepend"
                required
                placeholder="Внесете Име и Презиме"
              />
              <Form.Control.Feedback type="invalid" className="InvalidFeedback">
                Внесете Име и Презиме
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="" className="FormGroupCustom">
              <Form.Label>Емаил (задолжително)</Form.Label>
              <Form.Control type="" placeholder="Внесете емаил" required />
              <Form.Control.Feedback type="invalid" className="InvalidFeedback">
                Внесете Емаил
              </Form.Control.Feedback>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="btn-light" onClick={CloseJoinUs}>
              Исклучи
            </Button>
            <Button className="JoinUsButton" type="submit">
              <i class="fas fa-arrow-right JoinUsButtonPlus"></i> Испрати Форма
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
      <Navbar collapseOnSelect expand="md">
        <NavLink to="/">
          <img src={logo} alt="" />
        </NavLink>
        <Navbar.Toggle aria-controls="responsive-navbar-nav " />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <NavLink to="/spaceblog"  className="NavLinkCustom">
              Space блог
            </NavLink>
            <NavLink to="/events" className="NavLinkCustom">
              Настани
            </NavLink>
            <Nav.Link href="/#coworking" className="NavLinkCustom">
              Co-working
            </Nav.Link>
            <NavLink to="/academies" className="NavLinkCustom">
              Академии
            </NavLink>
            <NavLink to="/eventspace" className="NavLinkCustom">
              Простор за настани
            </NavLink>
            <Nav.Link href="/#partneri" className="NavLinkCustom">
              Партнерства
            </Nav.Link>
            <Nav.Link
              href="#"
              className="JoinUsLinkkk"
              onClick={ShowJoinUs}
              expand="xs"
            >
              <Button className="JoinUsButtonkk">
                <i className="fas fa-plus JoinUsButtonPlus"></i>Приклучи се
              </Button>
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
        <Nav.Link href="#" className="JoinUsLink" onClick={ShowJoinUs}>
          <Button className="JoinUsButton">
            <i className="fas fa-plus JoinUsButtonPlus"></i>Приклучи се
          </Button>
        </Nav.Link>
      </Navbar>
    </Container>
  );
};

export default Header;
