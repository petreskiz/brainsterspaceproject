import React from "react";
import "./Card.css";
import { Link } from "react-router-dom";
import { Card } from "react-bootstrap";
import Narrow from "../../../Sliki/arrow-right-circle (1).svg";

const Cards = props => {
  return (
    <div className="Card">
      {props.link &&
      (props.link.includes("http") || props.link.includes("#")) ? (
        <a
          href={props.link}
          target={props.link.includes("http") ? "_Blank" : ""}
          className="CardLink"
        >
          <Card>
            <Card.Img variant="top" src={props.picture} />
            <Card.Body className="CardBodyCustom">
              <Card.Title className="CardTitle">{props.title}</Card.Title>
              <Card.Text className="CardText">{props.text}</Card.Text>
            </Card.Body>
            <Card.Footer
              className="CardFooter"
              style={{
                justifyContent: props.children ? "space-between" : "flex-end"
              }}
            >
              <img src={Narrow} alt="" />
              {props.children}
            </Card.Footer>
          </Card>
        </a>
      ) : (
        <Link to={props.link} className="CardLink">
          <Card>
            <Card.Img variant="top" src={props.picture} />
            <Card.Body className="CardBodyCustom">
              <Card.Title className="CardTitle">{props.title}</Card.Title>
              <Card.Text className="CardText">{props.text}</Card.Text>
            </Card.Body>
            <Card.Footer
              className="CardFooter"
              style={{
                justifyContent: props.children ? "space-between" : "flex-end"
              }}
            >
              {props.children}
              <img src={Narrow} alt="" />
            </Card.Footer>
          </Card>
        </Link>
      )}
    </div>
  );
};

export default Cards;
