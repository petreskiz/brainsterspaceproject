import React from "react";
import "./HomePage.css";
import Picture from "../../Sliki/IMG_7707.jpg";
import { Container, Row, Col } from "react-bootstrap";
import AboutUs from './AboutUs/AboutUs'

const HomePage = () => {
  return (
    <div>
      <Container className="HomePageContainer">
        <Row>
          <Col
            className="HomePagePicture"
            style={{
              backgroundImage: `url(${Picture})`
            }}
          >
            <div className="HomePagePictureOpacity">
              <h1 className="HomePagePictureText">
                Центар за Учење, Кариера и <br /> Иновација
              </h1>
            </div>
          </Col>
        </Row>
            <AboutUs/>
      </Container>
    </div>
  );
};

export default HomePage;
