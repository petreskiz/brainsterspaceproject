import React from "react";
import "./AboutUs.css";
import { Row, Col, Button, Form, Badge } from "react-bootstrap";
import Cards from "../Card/Card";
import Picture1 from "../../../Sliki/partnerstvasotechkomp.jpg";
import Picture2 from "../../../Sliki/Za_Nas/coworking.jpg";
import Picture3 from "../../../Sliki/Za_Nas/edukacija1.jpg";
import Picture4 from "../../../Sliki/prostorzanastani.jpg";
import Picture5 from "../../../Sliki/Za_Nas/nastani.jpg";
import Picture6 from "../../../Sliki/IMG_7397.jpg";
import Picture7 from "../../../Sliki/Nastani/Hristijan-Nosecka-1024x536.jpg";
import Picture8 from "../../../Sliki/Nastani/IMG_7481.jpg";
import Picture9 from "../../../Sliki/Nastani/instruktori.jpg";
import { Modal } from "react-bootstrap";
import { useState } from "react";
import { Link } from "react-router-dom";

const AboutUs = props => {
  const [partner, setPartner] = useState(false);
  const handleClose = () => setPartner(false);
  const handleShow = () => setPartner(true);

  const [inovacii, setInovacii] = useState(false);
  const CloseInovacii = () => setInovacii(false);
  const ShowInovacii = () => setInovacii(true);

  const [coworking, setCoworking] = useState(false);
  const [education, setEducation] = useState(false);

  const [validated, setValidated] = useState(false);

  const handleSubmit = event => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    setValidated(true);
  };

  return (
    <Row className="AboutUsAllCards">
      <Modal show={partner} onHide={handleClose} className="PartnerModal">
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
          <Modal.Header closeButton>
            <Modal.Title>Приклучи се</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group
              controlId="validationCustomUsername"
              className="FormGroupCustom"
            >
              <Form.Label>Име и Презиме (задолжително)</Form.Label>
              <Form.Control
                type="text"
                aria-describedby="inputGroupPrepend"
                required
                placeholder="Внесете Име и Презиме"
              />
              <Form.Control.Feedback type="invalid" className="InvalidFeedback">
                Внесете Име и Презиме
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="">
              <Form.Label>Име на Компанијата (незадолжително)</Form.Label>
              <Form.Control placeholder="Внесете име на Вашата Компанија" />
            </Form.Group>
            <Form.Group controlId="" className="FormGroupCustom">
              <Form.Label>Телефонски Број (задолжително)</Form.Label>
              <Form.Control
                type="number"
                aria-describedby="inputGroupPrepend"
                required
                placeholder="Внесете Телефонски Број"
              />
              <Form.Control.Feedback type="invalid" className="InvalidFeedback">
                Внесете Телефонски број
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="">
              <Form.Label>Предлог за соработка (незадолжително)</Form.Label>
              <Form.Control
                as="textarea"
                placeholder="Во 300 карактери, опишете зошто саката да соработуваме"
                rows="3"
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="light" onClick={handleClose}>
              Исклучи
            </Button>
            <Button className="JoinUsButton" type="submit">
              <i className="fas fa-arrow-right JoinUsButtonPlus"></i> Испрати Форма
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
      <Modal show={inovacii} onHide={CloseInovacii} className="InovaciiModal">
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
          <Modal.Header closeButton>
            <Modal.Title>Приклучи се</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group
              controlId="validationCustomUsername"
              className="FormGroupCustom"
            >
              <Form.Label>Име и Презиме (задолжително)</Form.Label>
              <Form.Control
                type="text"
                aria-describedby="inputGroupPrepend"
                required
                placeholder="Внесете Име и Презиме"
              />
              <Form.Control.Feedback type="invalid" className="InvalidFeedback">
                Внесете Име и Презиме
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="">
              <Form.Label>Име на Компанијата (незадолжително)</Form.Label>
              <Form.Control
                type=""
                placeholder="Внесете име на Вашата Компанија"
              />
            </Form.Group>
            <Form.Group controlId="" className="FormGroupCustom">
              <Form.Label>Телефонски Број (задолжително)</Form.Label>
              <Form.Control
                type="number"
                aria-describedby="inputGroupPrepend"
                required
                placeholder="Внесете Телефонски Број"
              />
              <Form.Control.Feedback type="invalid" className="InvalidFeedback">
                Внесете Телефонски број
              </Form.Control.Feedback>
              <Form.Group controlId="" className="FormGroupCustom">
                <Form.Label>Емаил (задолжително)</Form.Label>
                <Form.Control required placeholder="Емаил" />
                <Form.Control.Feedback
                  type="invalid"
                  className="InvalidFeedback"
                >
                  Внесете Емаил
                </Form.Control.Feedback>
              </Form.Group>
            </Form.Group>
            <Form.Group controlId="">
              <Form.Label>Потреби на компанијата (незадолжително)</Form.Label>
              <Form.Control
                as="textarea"
                placeholder="Во 300 карактери, опишете зошто саката да соработуваме"
                rows="3"
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="light" onClick={handleClose}>
              Исклучи
            </Button>
            <Button className="JoinUsButton" type="submit">
              <i className="fas fa-arrow-right JoinUsButtonPlus"></i> Испрати Форма
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
      <Col lg={12} className="Margin-Top">
        <h1 className="AboutUsText">За Нас</h1>
      </Col>
      <Row className="">
        <Col md={4} sm={6} xs={12}>
          <Cards
            title="Едукација"
            picture={Picture3}
            text="Научи практични вештини за трансформација во кариерата. Нашата специјалност е да ти помогнеме да го најдеш правилниот skill set кој ќе одговари на реалните потреби на пазарот на труд. Организираме курсеви, академии и персонализирани обуки кои одговараат на реалните потреби на денешницата."
            link="https://brainster.co/"
          />
        </Col>
        <Col md={4} sm={6} xs={12}>
          <Cards
            title="Настани"
            picture={Picture5}
            link="/events"
            text="Специјално курирани и организирани настани кои ги поврзуваат правите таленти со иновативните компании. Идејата е да нашата Теch заедница расте, се инспирира и креира преку овие настани."
          />
        </Col>
        <Col md={4} sm={6} xs={12}>
          <Cards
            title="Coworking"
            picture={Picture2}
            text="Биди дел од Tech заедницата на иноватори креативци и претприемачи. Резервирај стол во нашата share office. Пичирај го твојот бизнис и нашиот тим заедно ќе одлучи секој месец со кого да ги дели своите канцеларии."
            link="#coworking"
          />
        </Col>

        <Col md={4} sm={6} xs={12}>
          <Cards
            title="Простор за настани"
            picture={Picture4}
            link="/eventspace"
            text="Имаш идеја за обука или настан од Теch областа? Ние имаме простор за реализација. Нашиот тим внимателно ги одбира и курира сите настани."
          />
        </Col>
        <Col md={4} sm={6} xs={12} onClick={handleShow}>
          <Cards
            title="Партнерство со Tech компании"
            picture={Picture1}
            text="Целта и идејата е креирање на Теch заедница која ќе се негува, расте и креира подобро утре за нас. Преку директно и индиректно поврзување на tech талентите и компаниите."
          />
        </Col>
        <Col md={4} sm={6} xs={12} onClick={ShowInovacii}>
          <Cards
            title="Иновации за компании"
            picture={Picture6}
            text="Нов концепт кој ќе им помогне на компаниите да преминат од стариот во новиот начин на работење. Подгответе ја вашата компанија за дигитална трансформација."
          />
        </Col>
      </Row>
      <Row className="EducationRow Margin-Top">
        <Col md={5} sm={12} xs={12} className="EducationLeft">
          {!education ? (
            <h2 className="EducationTitle">Едукација</h2>
          ) : (
            <h2 className="EducationTitle">Компании</h2>
          )}

          {education ? (
            <div>
              <p className="EducationText">
                Компаниите имаат можност да ги надоградат своите тимови, а со
                тоа да ги подобрат перформансите на својата компанија.
              </p>
              <p className="EducationText">
                Дигитална трансформација се случува, а вашите компании треба да
                бидат подготвени за да се адаптираат соодвентно. Обуки,
                семинари, курсеви или Team Building.
              </p>
              <p className="EducationText">
                Во Brainster Space имаме специјално обучен тим кој е подготвен
                да ја насочи и адаптира сподели својата експертиза со денешната
                потреба на компаниите.
              </p>
            </div>
          ) : (
            <p className="EducationText">
              {" "}
              Дали си подготвен да одговориш на потребите на иднината.
              Вистинските курсеви, академии и семинари кои ќе ти овозможат
              кариерна трансформација во областа дигитален маркетинг, дизајн,
              програмирање и Data Science.
            </p>
          )}

          <Button
            className="JoinUsButton"
            onClick={e => {
              e.preventDefault();
              setEducation(false);
            }}
          >
            Академии
          </Button>
          <Button
            onClick={e => {
              e.preventDefault();
              setEducation(true);
            }}
            className="JoinUsButton"
            style={{ marginLeft: "5px" }}
          >
            За Компании
          </Button>
        </Col>
        <Col md={7} sm={12} xs={12} className="">
          <img
            src={`${education ? Picture1 : Picture2}`}
            alt=""
            width="100%"
            style={{ borderRadius: "5px" }}
          />
        </Col>
      </Row>
      <Row className="EventRow Margin-Top">
        <Col lg={12}>
          <h1 className="AboutUsText">Настани</h1>
        </Col>
        <Col md={4} sm={6} xs={12}>
          <Cards
            title="Codeworks"
            picture={Picture8}
            text="Вистинскиот настан за сите tech таленти кои сакаат да кодираат и да научат што е ново во tech заедницата. Настан кој ги поврзува mid програмерите со IT компаниите. Deep Dive Intensive Seminar Ексклузивни семинари кои комбинираат три различни техники на учење."
          />
        </Col>
        <Col md={4} sm={6} xs={12}>
          <Cards
            title="Deep Dive into Marketing"
            picture={Picture9}
            text="Deep Dive into marketing се серија од интензивни семинари кои се наменски подготвени да ти помогнат да ги совладаш и предвидиш современите промени, предизвици и идните трендови во областа на маркетингот."
          />
        </Col>
        <Col md={4} sm={6} xs={12}>
          <Cards
            title="Deep Dive into Data Science"
            picture={Picture7}
            text="Deep Dive into Data Science се серија од интензивни семинари кои се наменски подготвени да ти помогнат да ги совладаш и предвидиш современите промени, предизвици и идните трендови во областа на Data Science."
          />
        </Col>
        <Col
          className="EventButton justify-content-md-end justify-content-sm-center"
          sm={12}
          xs={12}
        >
          <Link to="/events">
            <Button className="JoinUsButton">
              <i className="far fa-calendar-alt JoinUsButtonPlus"></i>Календар на
              настани
            </Button>
          </Link>
        </Col>
      </Row>
      <Row className="CoworkingRow Margin-Top">
        <Col md={7} sm={12} xs={12}>
          <img
            src={Picture2}
            alt=""
            width="100%"
            style={{ borderRadius: "5px" }}
          />
        </Col>
        <Col md={5} sm={12} xs={12} className="EducationLeft">
          <h2
            id="coworking"
            className={`EducationTitle  ${coworking ? "text-trans" : ""}`}
          >
            Coworking
          </h2>
          <p className={`EducationText  ${coworking ? "text-trans" : ""}`}>
            Биди дел од tech заедницата на иноватори, креативци и претприемачи.
            Резервирај стол во нашата shared office. Пичирај го твојот бизнис и
            нашиот тим заедно ќе одлучи секој месец со кого да ги дели своите
            канцеларии.
          </p>

          <Button
            className="JoinUsButton"
            onClick={e => {
              e.preventDefault();
              setCoworking(!coworking);
            }}
          >
            Резервирај место
          </Button>

          <br></br>
          {coworking ? (
            <Badge pill variant="warning" className="SoldOut">
              Местата се распродадени
            </Badge>
          ) : (
            ""
          )}
        </Col>
      </Row>
      <Row className="EducationRow Margin-Top">
        <Col md={5} sm={12} xs={12} className="EducationLeft">
          <h2 className="EducationTitle">Простор за настани</h2>
          <p className="EducationText">
            Можност за презентации, обуки, конференции, networking настани.
            Одбери ја просторијата која најмногу ќе одговара на твојата идеја.
            Го задржуваме правото да одбереме кој настан ќе се организира во
            нашиот Brainster Space.
          </p>
          <Link to="/eventspace">
            <Button className="JoinUsButton">
              <i className="fas fa-arrow-right JoinUsButtonPlus"></i>Види го
              просторот
            </Button>
          </Link>
        </Col>
        <Col md={7} sm={12} xs={12} className="">
          <img
            src={Picture4}
            alt=""
            width="100%"
            style={{ borderRadius: "5px" }}
          />
        </Col>
      </Row>
      <Row className="PartnersRow Margin-Top">
        <Col>
          <h1 className="EducationTitle" id="partneri">
            Партнери
          </h1>
          <h5>Имаш идеја? Отворени сме за соработка</h5>
          <Link to="/eventspace">
            <Button className="JoinUsButton">
              <i className="fas fa-arrow-right JoinUsButtonPlus"></i>Види го
              просторот
            </Button>
          </Link>
        </Col>
      </Row>
    </Row>
  );
};

export default AboutUs;
