import React from "react";
import "./App.css";
import Header from "./components/Header/Header";
import HomePage from "./components/HomePage/HomePage";
import Footer from "./components/Footer/Footer";
import SpaceBlog from './components/SpaceBlog/SpaceBlog';
import Events from './components/Events/Events';
import EventSpace from './components/EventSpace/EventSpace';
import Academies from "./components/Academies/Academies"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <Switch>
         <Route path="/" exact component={HomePage}/> 
         <Route path="/spaceblog" component={SpaceBlog}/> 
         <Route path="/events" component={Events}/> 
         <Route path="/eventspace" component={EventSpace}/> 
         <Route path="/academies" component={Academies}/> 
        </Switch>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
